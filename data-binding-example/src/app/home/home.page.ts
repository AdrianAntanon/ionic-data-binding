import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  myVar: string = 'Esto es mi variable';
  userName: string = 'desconocido';

  constructor() {}
}
